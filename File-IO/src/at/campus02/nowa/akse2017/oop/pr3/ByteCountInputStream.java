package at.campus02.nowa.akse2017.oop.pr3;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

public class ByteCountInputStream extends FilterInputStream {
	
	private int counter = 0;

	@Override
	public int read() throws IOException {
		int read = super.read();
		if (read != -1) {
			counter++;
		}
		return read; 
	}

	@Override
	public int read(byte[] b, int off, int len) throws IOException {
		int read = super.read(b, off, len);
		if (read != -1) {
			counter += read;
		}
		return read;
	}

	@Override
	public int read(byte[] b) throws IOException {
		int read = super.read(b);
		if (read != -1) {
			counter += read;
		}
		return read;
	}

	public int getCounter() {
		return counter;
	}

	public ByteCountInputStream(InputStream arg0) {
		super(arg0);
	}
	

}
