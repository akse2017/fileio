package at.campus02.nowa.akse2017.oop.pr3;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Übung3 {

	public static void main(String[] args) {
		try (BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
			String line;
			while ((line = br.readLine()) != null) {
				if (line.equals("STOP")) {
					return;
				}
				System.out.println(line.toUpperCase());
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println("Diese Zeile steht am Ende!");

	}

}
