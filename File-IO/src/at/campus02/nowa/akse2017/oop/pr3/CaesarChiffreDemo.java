package at.campus02.nowa.akse2017.oop.pr3;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class CaesarChiffreDemo {

	public static void main(String[] args) {
		String text = "Hallo Welt!";
		try (CaesarChiffreOutputStream cos = new CaesarChiffreOutputStream(new FileOutputStream("geheim.txt"), 13)) {
			for (char c : text.toCharArray()) {
				cos.write(c);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		for (int x = 1; x < 50; x++) {
			System.out.print(x + ": ");
			try (CaesarChiffreInputStream cis = new CaesarChiffreInputStream(new FileInputStream("geheim.txt"), x)) {
				int b;
				while ((b = cis.read()) != -1) {
					char c = Character.toChars(b)[0];
					System.out.print(c);
				}
				System.out.println();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

}
