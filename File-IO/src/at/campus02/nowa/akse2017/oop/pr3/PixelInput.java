package at.campus02.nowa.akse2017.oop.pr3;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

public class PixelInput {

	public static void main(String[] args) {
		int count = 0;
		int red = 0;
		int green = 0;
		int blue = 0;
		try(InputStream is = new FileInputStream("screen.bmp")) {
			byte[] rgb = new byte[3];
			while (is.read(rgb) != -1) {
				count++;
				red += Math.abs(rgb[0]);
				green += Math.abs(rgb[1]);
				blue += Math.abs(rgb[2]);
			}
			System.out.println("Pixel: " + count);
			System.out.println("Red: " + red / count);
			System.out.println("Green: " + green / count);
			System.out.println("Blue: " + blue / count);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
