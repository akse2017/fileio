package at.campus02.nowa.akse2017.oop.pr3;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class FileInputStreamDemo {

	public static void main(String[] args) {
		/*File demo = new File("demo.txt");
		if (!demo.exists()) {
			System.out.println("Fehler, Datei existiert nicht!");
		}*/
		try (FileInputStream ins = new FileInputStream("demo.txt")) {
			
			int b;
			while((b = ins.read()) != -1) {
				System.out.println(Character.toChars(b)[0]);
			}
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		System.out.println(Integer.BYTES);

	}

}
