package at.campus02.nowa.akse2017.oop.pr3;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Date;

public class Übung2 {

	public static void main(String[] args) {
		String fileName = "object.txt";
		try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(fileName));
				ObjectInputStream ois = new ObjectInputStream(new FileInputStream(fileName))) {
			Person p = new Person("Michael", "Fladischer", new Date());
			oos.writeObject(p);
			oos.flush();
			
			Person pi = (Person)ois.readObject();
			System.out.println(pi.getNachname());
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
