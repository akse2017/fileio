package at.campus02.nowa.akse2017.oop.pr3;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

public class BufferedInputStreamDemo {

	public static void main(String[] args) {
		try (FileInputStream fis = new FileInputStream("demo2.txt");
				BufferedInputStream bis = new BufferedInputStream(fis)) {
			byte[] ba = new byte[10];
			int r = bis.read(ba);
			System.out.println(r);
			int b;
			while ((b = bis.read()) != -1) {
				char c = Character.toChars(b)[0];
				System.out.println(c);
			}
			BufferedReader br = new BufferedReader(new InputStreamReader(bis));
			br.readLine();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
