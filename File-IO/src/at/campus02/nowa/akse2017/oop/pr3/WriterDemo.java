package at.campus02.nowa.akse2017.oop.pr3;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.StandardCharsets;

public class WriterDemo {

	public static void main(String[] args) {
		try (Writer fw = new OutputStreamWriter(new FileOutputStream("notes.txt"), StandardCharsets.UTF_8)) {
			fw.write("Hallo��� Welt!\n");
			try {
				Integer i = Integer.parseInt("sdlkfjasl�dfjdl�f");
				fw.write(i);
			} catch (NumberFormatException e) {
				System.out.println("Keine Zahl!!!");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
