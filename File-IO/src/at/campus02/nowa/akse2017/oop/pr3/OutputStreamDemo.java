package at.campus02.nowa.akse2017.oop.pr3;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class OutputStreamDemo {

	public static void main(String[] args) {
		try (FileOutputStream fos = new FileOutputStream("demo2.txt", true)) {
			fos.write(Integer.decode("0x41"));
			Thread.sleep(1000*5);
			fos.flush();
			Thread.sleep(1000*5);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

	}

}
