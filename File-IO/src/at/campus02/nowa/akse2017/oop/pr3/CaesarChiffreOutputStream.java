package at.campus02.nowa.akse2017.oop.pr3;

import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class CaesarChiffreOutputStream extends FilterOutputStream {
	
	private final int key;

	public CaesarChiffreOutputStream(OutputStream arg0, int key) {
		super(arg0);
		this.key = key;
	}

	@Override
	public void write(int b) throws IOException {
		super.write(b + this.key);
	}
	
	

}
