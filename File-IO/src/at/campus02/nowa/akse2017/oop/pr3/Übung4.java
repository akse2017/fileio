package at.campus02.nowa.akse2017.oop.pr3;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.util.regex.Pattern;

public class Übung4 {

	public static void main(String[] args) {
		try (BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
				PrintWriter pw = new PrintWriter(new OutputStreamWriter(new FileOutputStream("benotung.csv"), StandardCharsets.UTF_8))) {
			
			
			
			
			
			
			Pattern pname = Pattern.compile("[^;]+");
			Pattern pnote = Pattern.compile("[1-5]");
			while (true) {
				System.out.println("Name: ");
				String name = br.readLine();
				if (name == null || name.equals("STOP")) {
					break;
				}
				if (!pname.matcher(name).matches()) {
					System.out.println("Falsche Eingabe!");
					continue;
				}
				System.out.println("Note:");
				String note = br.readLine();
				if (note == null || note.equals("STOP")) {
					break;
				}
				if (!pnote.matcher(note).matches()) {
					System.out.println("Falsche Eingabe!");
					continue;
				}
				pw.println(name + ";" + note);
				pw.flush();
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}

	}

}
