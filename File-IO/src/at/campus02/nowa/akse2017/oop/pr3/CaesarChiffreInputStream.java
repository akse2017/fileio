package at.campus02.nowa.akse2017.oop.pr3;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

public class CaesarChiffreInputStream extends FilterInputStream {
	
	private final int key;

	public CaesarChiffreInputStream(InputStream in, int key) {
		super(in);
		this.key = key;
	}

	@Override
	public int read() throws IOException {
		int read = super.read();
		if (read != -1) {
			return read - key;
		}
		return read;
	}
	
	

}
