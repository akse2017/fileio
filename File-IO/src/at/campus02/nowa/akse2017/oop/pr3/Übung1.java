package at.campus02.nowa.akse2017.oop.pr3;

import java.io.IOException;

public class Übung1 {

	public static void main(String[] args) {
		int b;
		try {
			while((b = System.in.read()) != -1) {
				char c = Character.toChars(b)[0];
				if (c == 'x') {
					return;
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
