package at.campus02.nowa.akse2017.oop.pr3;

import java.io.File;
import java.io.IOException;

public class FileDemo {

	public static void main(String[] args) {
		File demo = new File("demo.txt");
		if (!demo.exists()) {
			try {
				demo.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		System.out.println("Ordner: " + demo.getAbsoluteFile().getParent());
		System.out.println("Gr��e: " + demo.length());
		System.out.println("Versteckt: " + demo.isHidden());
		System.out.println("Frei: " + demo.getFreeSpace()/1024/1024);
		System.out.println("Path-Separator: " + File.pathSeparator);
		System.out.println("Separator: " + File.separator);
		
		for (File f : demo.getAbsoluteFile().getParentFile().listFiles()) {
			System.out.println(" * " + f.getName() + (f.isDirectory() ? File.separator : ""));
		}

	}

}

