package at.campus02.nowa.akse2017.oop.pr3;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

public class ByteCountFilterDemo {

	public static void main(String[] args) {
		try (ByteCountInputStream is = new ByteCountInputStream(new FileInputStream("benotung.csv"))) {
			int b;
			while ((b = is.read()) != -1) {
				System.out.println(b);
			}
			System.out.println("Gelesen: " + is.getCounter());
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
